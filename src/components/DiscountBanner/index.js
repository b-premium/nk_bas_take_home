import React from 'react';
import './style/style.scss';

const DiscountBanner = () => {
    return (
        <div className="bas-discount-container">
            <header className="w3-container w3-beige">
                <p className="bas-discount-text">87% Off Canvas Prints! Click for Details</p>
            </header>
        </div>
    )
};

export default DiscountBanner;