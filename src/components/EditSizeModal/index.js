

import React from 'react';
import './style/w3.css';

const EditSizeModal = ({hideModal}) => {
    return (
        <div id="id01" className="w3-modal">
            <div className="w3-modal-content">
                <div className="w3-container">
            <span onClick={hideModal}
                  className="w3-button w3-display-topright">&times;</span>
                    <p>Some text. Some text. Some text.</p>
                    <p>Some text. Some text. Some text.</p>
                </div>
            </div>
        </div>
    )
};

export default EditSizeModal;