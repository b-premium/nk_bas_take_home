import React from 'react';
import './style/style.scss';
import ButtonImage from './ButtonImage';
import editSvg from "./assets/edit.svg";
import gearSvg from "./assets/gear.svg";
import rotateSvg from "./assets/rotate.svg";
import saveSvg from "./assets/save.svg";
import uploadSvg from "./assets/upload.svg";

const ImageOperationButtons = ({edit, upload, rotate, save, editSize}) => {
    return (
        <div className="bas-image-operations-container">
            <div>
                <div>
                    <ButtonImage btnClick={edit}
                                 imgSrc={editSvg}
                                 imgAlt="Edit Current Item"
                                 h4Text="Edit &emsp;" />
                    <ButtonImage btnClick={upload}
                                 imgSrc={uploadSvg}
                                 imgAlt="Upload Remote Content"
                                 h4Text="Upload" />
                    <ButtonImage btnClick={rotate}
                                 imgSrc={rotateSvg}
                                 imgAlt="Rotate Current Image"
                                 h4Text="Rotate" />
                    <ButtonImage btnClick={save}
                                 imgSrc={saveSvg}
                                 imgAlt="Save"
                                 h4Text="Save" />
                    <ButtonImage btnClick={editSize}
                                 imgSrc={gearSvg}
                                 imgAlt="Edit Size"
                                 h4Text="Edit Size" />
                </div>
            </div>
        </div>

    )
};

export default ImageOperationButtons;