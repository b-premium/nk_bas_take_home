import React from 'react';
import './style/style.scss';

const ClickToUploadImageDiv = ({imgSrc, btnClick, imgAlt, h4Text}) => {
    return (
        <button className="bas-operation-btn" onClick={btnClick}>
            <img src={imgSrc}
                 width="90" height="90"
                 className="bas-green-svg-color"
                 alt={imgAlt} />
            <h4>{h4Text}</h4>
        </button>
    )
};

export default ClickToUploadImageDiv;