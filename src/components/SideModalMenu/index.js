import React, { Component } from 'react';
import './styles/modalMenu.scss';

class SideModalMenu extends Component {

    deriveShowModalClass(showMyMenu) {
        const tempClass = "overlay";
        if(showMyMenu){
            return `${tempClass} showOverlay`
        }
        return `${tempClass} hideOverlay`
    }

    render() {
        const showMyMenu = this.props.showMenu;
        const modalClass = this.deriveShowModalClass(showMyMenu);
        return (
            <div id="myNav" className={modalClass}>
                <a href="javascript:void(0)" className="closebtn" onClick={this.props.closeNav}>&times;</a>
                <div className="overlay-content">
                    <a href="#">About</a>
                    <a href="#">Services</a>
                    <a href="#">Clients</a>
                    <a href="#">Contact</a>
                </div>
            </div>
        );
    }
}

export default SideModalMenu;

