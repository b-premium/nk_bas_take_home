import React, { Component} from 'react';
import './styles/style.scss';
import hamburgerMenuPng from './assets/hamburger_menu.png';
import logoSvg from './assets/logo.svg';
import cartSvg from './assets/cart.svg';
import helpSvg from './assets/help.svg';

class LogoMenuContainer extends Component {

    render() {
        // const sideModalMenu = <SideModalMenu showMenu={this.state.showMenu} closeNav={this.closeNav.bind(this)}/>;
        return (
            <div className="bas-logo-menu-container">
                <div className="bas-hamburger-logo">
                    <img src={hamburgerMenuPng}
                         width="52"
                         height="52"
                         className="bas-menu-ham"
                         alt="Hambuger Menu"
                         onClick={this.props.openNav} />

                        <img src={logoSvg}
                             width="60"
                             height="60"
                             className="bas-logo"
                             alt="Company Logo" />
                </div>
                <div className="bas-blank-nav-space">
                </div>
                <div className="bas-help-cart">
                    <img src={helpSvg}
                         width="60"
                         height="60"
                         className="bas-beige-svg-color"
                         alt="Help" />
                        <img src={cartSvg}
                             width="60"
                             height="60"
                             className="bas-beige-svg-color"
                             alt="Shopping Cart" />
                </div>
            </div>

        )
    }





}

export default LogoMenuContainer;