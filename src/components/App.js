import React, { Component } from 'react';
import $ from 'jquery';
import SideModalMenu from './SideModalMenu';
import LogoMenuContainer from './LogoMenuContainer';
import DiscountBanner from './DiscountBanner';
import Footer from './Footer';
import ClickToUploadImageDiv from './ClickToUploadImageDiv';
import ImageOperationButtons from './ImageOperationButtons';
import EditSizeModal from './EditSizeModal';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMenu: false,
            rotateDegrees: 0,
            isImageLoaded: false,
            uploadImageId: '#bas-upload-image-div',
            remoteImageId: '#bas-remote-image-div',
            remoteImageUrl: 'https://source.unsplash.com/random',
            imageId: '#bas-loaded-image'
        };
    }

    componentDidMount() {

        this.setState({
            rotateDegrees: 0,
            isImageLoaded: false
        });

        $(this.state.uploadImageId).removeClass("bas-hide-div");
        $(this.state.remoteImageId).addClass("bas-hide-div");
    }

    setShowMenuState = (showMenu) => {
        this.setState({
            showMenu});
   };

    openNav = () => {
        this.setShowMenuState(true);
    };

    closeNav = () => {
        this.setShowMenuState(false);
    };

    showImageDivHideUploadImageDiv = () => {
        console.log('I see showImageDivHideUploadImageDiv()');
        $(this.state.uploadImageId).addClass("bas-hide-div");
        $(this.state.remoteImageId).removeClass("bas-hide-div");
    };

    upload = () => {

        const reactThis = this;
        $(this.state.imageId).attr('src', this.state.remoteImageUrl)
            .on('load', function() {
                if (!this.complete || typeof this.naturalWidth === "undefined" || this.naturalWidth === 0) {
                    alert('broken image!');
                } else {

                    reactThis.showImageDivHideUploadImageDiv();
                }
            });

    };

    rotate = () => {
        const tempNumber = this.state.rotateDegrees + 90;
        const transformationString = 'rotate(-' + tempNumber + 'deg)';
        $(this.state.imageId).css({"-webkit-transform" : transformationString});
        this.setState( {rotateDegrees: tempNumber} )
    };

    editSize = () => {
        $("id01").css("display","block");
    };

    hideEditSizeModal = () => {
        $("id01").css("display","none");
    };

    render() {
        const sideModalMenu = <SideModalMenu showMenu={this.state.showMenu} closeNav={this.closeNav}/>;
        const discountBanner = <DiscountBanner />
        const logoMenuContainer = <LogoMenuContainer openNav={this.openNav} />;
        const imageOperationButtons = <ImageOperationButtons upload={this.upload}
                                                             rotate={this.rotate}
                                                             editSize={this.editSize} />;
        const clickToUploadImageDiv = <ClickToUploadImageDiv upload={this.upload} />;
        const footer = <Footer upload={this.upload} />;
        const editSizeModal = <EditSizeModal hideModal={this.hideEditSizeModal} />
        return (
            <div>
                {logoMenuContainer}
                <br/>
                {discountBanner}
                <br/>
                {imageOperationButtons}
                <br/>
                {clickToUploadImageDiv}
                <br/>
                {footer}
                {sideModalMenu}
                {editSizeModal}
            </div>
        )
    }
}

export default App;