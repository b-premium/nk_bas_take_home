import React from 'react';
import './style/style.scss';

const Footer = ({upload}) => {
    return (
        <div className="navbar navbar-fixed-bottom bas-footer-line">
            <div className="bas-logo-menu-container">
                <div className="bas-price-area">
                    <h2 className="bas-discount-price">$17.60</h2>
                    <p className="bas-discount-price-original">$135.39</p>
                </div>
                <div className="bas-blank-nav-space">
                </div>
                <div className="bas-upload-button-placement">
                    <input type="button"
                           className="bas-upload-button"
                           value="Upload"
                           onClick={upload} />
                </div>
            </div>
        </div>
    )
};

export default Footer;