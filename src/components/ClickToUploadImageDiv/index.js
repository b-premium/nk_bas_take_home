import React from 'react';
import './style/style.scss';
import uploadPng from "./assets/upload.png";
require('typeface-noto-sans')

const ClickToUploadImageDiv = ({upload}) => {
    return (
        <div className="bas-click-to-upload-image-container">
            <div id="bas-upload-image-div" className="bas-click-to-load-image" onClick={upload}>
                <img src={uploadPng} className="bas-beige-svg-color" alt="upload"/>
                    <p className="bas-t3-noto-sans-bold">Click To Upload Image</p>
                    <p className="bas-t4-noto-sans">Accepted file types .jpg, .png, .gif</p>
            </div>
            <div id="bas-remote-image-div" className="bas-click-to-load-image bas-hide-div">
                <img id="bas-loaded-image" width="40%" alt="Remote content" />
            </div>
        </div>

    )
};

export default ClickToUploadImageDiv;